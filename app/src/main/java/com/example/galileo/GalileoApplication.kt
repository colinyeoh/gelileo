package com.example.galileo

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GalileoApplication: Application()