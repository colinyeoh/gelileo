package com.example.galileo.ui.main.repo

import android.content.Context
import com.example.galileo.R
import com.example.galileo.ui.main.model.Quotes
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class Repository @Inject constructor(@ApplicationContext val context: Context) {

    fun getQoutesFromResources(): Quotes {
        val br = context.resources.openRawResource(R.raw.cowbow_wisdoms).bufferedReader().use { it.readText() }
        return Gson().fromJson(br, Quotes::class.java)
    }
}