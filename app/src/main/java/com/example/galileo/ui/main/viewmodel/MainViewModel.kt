package com.example.galileo.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.galileo.ui.main.model.Quotes
import com.example.galileo.ui.main.repo.Repository
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository): ViewModel() {

    private val _state = MutableLiveData<MainViewState>()
    val state = _state

    fun getQuotes() {
        viewModelScope.launch {
            runCatching {
                repository.getQoutesFromResources().quotes.shuffled().first().let { qoute ->
                    _state.value = MainViewState.OnContentLoaded(qoute)
                }
            }.onFailure {
                _state.value = MainViewState.OnError(it)
            }
        }
    }

    sealed class MainViewState {
        data class OnContentLoaded(val quote: String): MainViewState()
        data class OnError(val error: Throwable): MainViewState()
    }
}