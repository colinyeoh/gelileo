package com.example.galileo.ui.main.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.galileo.R
import com.example.galileo.databinding.MainFragmentBinding
import com.example.galileo.ui.main.model.Quotes
import com.example.galileo.ui.main.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding
    private var quotes: Quotes? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        initUiElements()
        initObservers()
    }

    private fun initUiElements() {
        binding.button.setOnClickListener {
            viewModel.getQuotes()
        }
    }

    private fun initObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MainViewModel.MainViewState.OnContentLoaded -> {
                    binding.message.text = state.quote
                }
                is MainViewModel.MainViewState.OnError -> {
                    Toast.makeText(requireContext(), R.string.cowbow_fell, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}